const path = require("path");

module.exports = {
  entry: ["/index.js"],
  output: {
    filename: "./js/bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, "src/js"),
        use: {
          loader: "babel-loader",
          options: {
            presets: "env"
          }
        }
      }
    ]
  },
  plugins: []
};
