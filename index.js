// 1) создать класс Employee - конструктор принимает такие параметры как имя, фамилия, возраст, количество отработанных часов в месяц, у этого класса должны быть методы для получения полного имени и зарплаты в месяц посчитанной по базовой ставке ЗП = колво часов * 1 * 20$;
// 2) создать класс Programmer который также является Employee, но за считается по формуле зп = колво часов * 1.2 * 20$; 
// 3) создать класс Manager который также является Employee, но за считается по формуле зп = колво часов * 1.5 * 20$;
// 4) Сгенерить отчет в консоль браузера о зарплатах всех сотрудников компании ввиде:
// ` Mykyta 777 - 2200$ 
//   Denis Ilkin - 1500$
//   tOTAL - 13000$ `   (перенос строки если что \n)
// 5) Выборка сотрудников:
const employees = [{
        type: 'employee',
        age: 21,
        hours: 40,
        firstName: 'John',
        lastName: 'Smith'
    },
    {
        type: 'programmer',
        age: 25,
        hours: 60,
        firstName: 'Mykyta',
        lastName: '777'
    },
    {
        type: 'programmer',
        age: 26,
        hours: 140,
        firstName: 'Denis',
        lastName: 'Ilkin'
    },
    {
        type: 'programmer',
        age: 26,
        hours: 50,
        firstName: 'Yuri',
        lastName: 'Shpak'
    },
    {
        type: 'manager',
        age: 41,
        hours: 40,
        firstName: 'Johna',
        lastName: 'Smithy'
    },
    {
        type: 'employee',
        age: 25,
        hours: 20,
        firstName: 'Oleg',
        lastName: 'Tsar'
    },
]

const moduleGenerateReport = require('./modules/generateReport')(employees);
moduleGenerateReport.getReport()